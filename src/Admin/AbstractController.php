<?php
namespace Project\Admin;

use Project\Security\ConnexionManager;
use Project\PageManager;

abstract class AbstractController {
    protected $baseRoot;

    protected $internalExpose = [];

    public function getPage($mode = false, $exposedVars = []) {
        if(!$mode) { $mode = ConnexionManager::getRequest('mode'); }

        $page = PageManager::getRouter()->getRoute(PageManager::getPage())->getFile();

        if(!$page) { return ''; }

        $fileRoot = sprintf('./page/%s/%s.php', $page, $mode);

        if(!file_exists($fileRoot)) { return ''; }


        $exposedVars = array_merge($exposedVars, $this->internalExpose, ConnexionManager::getFromSession('var_exposed'));
        foreach ($exposedVars as $var => $value) {
            ${$var} = $value;
        }
        include($fileRoot);

        $this->internalExpose = [];
    }

    public function getController($mode = false) {
        if(!$mode) { $mode = ConnexionManager::getRequest('mode'); }
        $controllerName = str_replace('-','',$mode).'Controller';

        if(method_exists(static::class, $controllerName)) {
            $this->{$controllerName}();
        }
    }

}