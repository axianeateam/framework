<?php
namespace Project\Admin;


use Project\Security\ConnexionManager;
use Project\PageManager;

abstract class AbstractTrinityController extends AbstractController {
    protected $baseRoot;

    protected $internalExpose = [];

    abstract public function addController();
    abstract public function editionController();
    abstract public function removeController();
}