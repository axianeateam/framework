<?php
namespace Project\Dao;

use Project\Models\AbstractDbClass;
use Project\Security\ConnexionManager;
use Project\Utilities\ErrorCatcher;
use Project\Utilities\SimpleDataPerfTest;
use Project\Utilities\SimpleTimePerfTest;
use Project\Utilities\SqlPerfTest;
use ReflectionClass;

class AbstractDao
{
    /**
     * @var \mysqli $mysqli
     */
    protected $mysqli;
    protected $tableName;
    protected $modelName;
    protected $multipleName;

    /**
     * @return mixed
     */
    public function getMultipleName()
    {
        if(!$this->multipleName) {
            return $this->modelName.'s';
        }
        return $this->multipleName;
    }

    public function getClassShortName()
    {
        $reflect = new ReflectionClass($this);
        return $reflect->getShortName();
    }

    public function getDir()
    {
        return __DIR__;
    }

    public function isInError($message = ''): bool
    {
        if(!$this->mysqli->error_list) { return false; }

        $env = getenv('ENV_MODE');
        if( $env == 'prod' || $env == 'master' || !$env ) { return true; }

        ConnexionManager::addFlashSessionMessage('danger', $message);
        foreach ($this->mysqli->error_list as $error) {
            ConnexionManager::addFlashSessionMessage('danger', sprintf('DB_ERROR_%s : %s', $error['errno'], $error['error']));
        }

        return true;
    }

    public function customSelect($select, $from, $condition = '', $additional = '')
    {
        $Perfomance = new SimpleTimePerfTest();

        $buildedCond = $this->buildCondition($condition);

        $request  = sprintf('SELECT %s FROM %s WHERE %s %s', $select, $from, $buildedCond, $additional);
        $result = $this->mysqli->query($request);
        if($this->isInError('#DB_ERROR_SELECT: Erreur dans le chargement des données de '.$from)) {
            ErrorCatcher::addClassErrorMessage($this::getClassShortName(),'customSelect',['error' => $this->mysqli->error], new \Error());
            return [];
        }

        SqlPerfTest::addQuery('', $from,'customSelect['.$buildedCond.']', $Perfomance->getTestResult());
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function customUpdate($update, $table, $condition, $additional = '')
    {
        $updateBuilded = $update;
        if(is_array($update)) { $updateBuilded = implode(', ', $update); }
        $request = sprintf('UPDATE %s SET %s WHERE %s %s', $table, $updateBuilded, $this->buildCondition($condition), $additional);
        $result = $this->mysqli->query($request);
        if($this->isInError('#DB_ERROR_SELECT: Erreur dans la mise à jour des données de '.$table) || !$result) {
            ErrorCatcher::addClassErrorMessage($this::getClassShortName(),'customSelect',['error' => $this->mysqli->error], new \Error());

            return false;
        }
        return true;
    }


    public function buildCondition($conditions)
    {
        if($conditions) {
            if(is_array($conditions)) { return sprintf(' %s ', implode(' AND ', $conditions)); }
            return sprintf(' %s ', $conditions);
        }
        return ' 1 ';
    }

    protected function getModelsNamespace(): string
    {
        return 'Project\\Models\\';
    }
    protected function getModelFullNamespace(): string
    {
        return sprintf('%s%s', $this->getModelsNamespace(), $this->modelName);
    }

    protected function getModelMultipleName(): string
    {
        if($this->multipleName) { return $this->multipleName; }
        return $this->modelName.'s';
    }

    protected function abstractGet($parameters, $builder)
    {
        $Performance = new SimpleTimePerfTest();
        $modelFullName = $this->getModelFullNamespace();

        $methodFullName = 'get'.$this->modelName;
        if(!$builder['isSingle']) { $methodFullName = 'get'.$this->getModelMultipleName(); }

        if($parameters === []) { $parameters = []; }
        if(!is_array($parameters)) { $parameters = [$parameters]; }

        $advancedCache = DynamicCache::getResultsFromAdvancedCache($this->modelName, $parameters);
        if($advancedCache) {
            if(is_array($advancedCache) && count($advancedCache) === 1) { $advancedCache = $advancedCache[0]; }
            SimpleDataPerfTest::incrementData('dynamic_cache_object', 1);
            DynamicCache::$Count++;

            return $advancedCache;
        }

        $buildAdditionals = implode(' ', $builder['additionals']);
        $request = sprintf(
            "SELECT * FROM `%s` WHERE %s %s",
            $this->tableName,
            $this->buildCondition($parameters),
            $buildAdditionals
        );


        $result = $this->mysqli->query($request);
        if($this->isInError('#DB_ERROR_SELECT: Erreur dans la récupération des '.$this->tableName) || !$result) {
            ErrorCatcher::addClassErrorMessage($this::getClassShortName(),'get'.$modelFullName,['error' => $this->mysqli->error], new \Error());
            return [];
        }
        $result = $result->fetch_all(MYSQLI_ASSOC);

        $Objects = [];
        foreach ($result as $i => $element) {
            /** @var AbstractDbClass $Object */
            $Object = new $modelFullName();
            $Object->bindParametersToObject($element);

            $Objects[] = $Object;
        }

        SqlPerfTest::addQuery(
            $this->getClassShortName(),
            $this->tableName,
            $methodFullName,
            $Performance->getTestResult()
        );


        if(!$Objects) { return []; }

        //We store to the Dynamic Cache the results !

        DynamicCache::addToDynamicAdvancedCache($this->modelName, $Objects, $this->buildCondition($parameters));
        foreach ($Objects as $Object) {
            DynamicCache::addToDynamicCache($this->modelName,  $Object, $this->buildCondition($parameters));
        }

        if($builder['isSingle']) {return $Objects[0]; }
        return $Objects;
    }

    protected function abstractUpdate(AbstractDbClass $model, $builder)
    {
        $namespace = $this->getModelFullNamespace();
        if(!$model instanceof $namespace) { return false; }
        if($model->getId() && $builder['prefix'] == 'create') { $builder['prefix'] = 'update'; }

        $Performance = new SimpleTimePerfTest();

        $modelFullName = $this->getModelFullNamespace();
        $methodFullName = $builder['prefix'].$this->modelName;
        if(!$builder['isSingle']) { $methodFullName = $builder['prefix'].$this->getModelMultipleName(); }

        $allowedProps = $model->getAllowedProperties($builder['prefix']);
        $updateReq = [];
        $buildedReq = '';
        $cond = '';
        $prefixRequest = strtoupper($builder['prefix']);

        if($builder['prefix'] == 'update') {
            foreach ($allowedProps as $prop) {
                $updateReq[] = sprintf('`%s` = "%s"', $prop, $this->mysqli->escape_string($model->dynamicGetterByProp($prop)));
            }
            $buildedReq = 'SET '. implode(',', $updateReq);
            $cond = sprintf(' WHERE id = %d ', $model->getId());
        } else if($builder['prefix'] == 'create') {
            $prefixRequest = ' INSERT INTO ';
            $valuesReq = [];
            foreach ($allowedProps as $prop) {
                $updateReq[] = "`".$prop."`";
                $valueGet = $model->dynamicGetterByProp($prop);
                if(is_null($valueGet)) {
                    $valuesReq[] = 'NULL';
                    continue;
                }
                $valuesReq[] = '"' . $this->mysqli->escape_string($valueGet) . '"';
            }
            $buildedReq = sprintf('(%s) VALUES (%s)', implode(',',$updateReq), implode(',',$valuesReq));
        }

        $request = sprintf("  %s `%s` %s  %s ",
            $prefixRequest,
            $this->tableName,
            $buildedReq,
            $cond
        );

        $result = $this->mysqli->query($request);
        if($this->isInError('#DB_ERROR_SELECT: Erreur de mise à jour de '.$this->tableName) || !$result) {
            ErrorCatcher::addClassErrorMessage($this::getClassShortName(),$builder['prefix'].$modelFullName,['error' => $this->mysqli->error], new \Error());

            return false;
        }

        SqlPerfTest::addQuery(
            $this->getClassShortName(),
            $this->tableName,
            $methodFullName,
            $Performance->getTestResult()
        );

        if($builder['prefix'] == 'create') {
            if(method_exists($model, 'setId')) {
                $model->setId($this->mysqli->insert_id);
            }
            return $this->mysqli->insert_id;
        }

        return true;
    }

    protected function constructAbstractBuilder($methodName)
    {
        $builder = [
            'prefix' => '',
            'methodName' => $methodName,
            'additionals' => [],
            'searchByProps' => false,
            'isSingle' => true,
            'isValid' => false
        ];

        $searchRegex = sprintf('/(get|create|update)(%s|%s)(By([A-z]+))*/', $this->getModelMultipleName(), $this->modelName);

        /**
         * array $matches :
         * 0 => All group catched
         * 1 => prefix
         * 2 => ModelName called (single or multiple)
         * 3 => By{Propname} if specified
         * 4 => PropertyName called for the conditions
         */
        preg_match($searchRegex, $methodName, $matches);
        if(!$matches) { return $builder; }

        $builder['prefix'] = $matches[1];

        if(isset($matches[2])) {
            if($matches[2] == $this->getModelMultipleName()) { $builder['isSingle'] = false; }
        }

        if(isset($matches[3]) && isset($matches[4])) {
            $builder['searchByProps'] = $matches[4];
        }

        $builder['isValid'] = true;
        return $builder;
    }

    /**
     *
     * @param $methodName . is the called method that doesn't exist on the current Object.
     * @param $arguments . is every parameters in method(a,b,c...)
     * We First search if this is  a single call or multiple call.
     * If not, we return and save an error @see ErrorCatcher
     * Then, we'll call abstractGet with the gathered parameters.
     *
     * @return array|AbstractDbClass|bool
     */
    public function __call($methodName, $arguments)
    {
        $modelFullName = $this->getModelFullNamespace();

        //Security Tests
        if (!class_exists($modelFullName)) { return []; }
        if (!new $modelFullName() instanceof AbstractDbClass) { return []; }


        //Create the Query Builder
        $builder = $this->constructAbstractBuilder($methodName);
        if (!$builder['isValid']) {
            $error = new \Error();
            ErrorCatcher::addClassErrorMessage($this->getClassShortName(), $methodName, $arguments, $error);
            return [];
        }

        /*
         * We Test if the Query is a Create or an Update, if true we can query directly.
         * If not, we need to construct query's arguments/conditions
         */
        if($builder['prefix'] == 'create' || $builder['prefix'] == 'update') {
            if(!isset($arguments[0])) { return; }
            $model = $arguments[0];
            if(!$model instanceof AbstractDbClass) { return; }

            return $this->abstractUpdate($model, $builder);

        }
        if($builder['prefix'] != 'get') { return false; }

        /**
         * Clean Parameters before use.
         * Prevent errors.
         */

        if (is_array($arguments) && count($arguments) === 1 && !is_array($arguments[0])) { $arguments = $arguments[0]; }
        $conditions = [];
        $additionals = [];


        /*
         * CONDITIONS PARTS
         * Important : We need to slice the conditionals parts after assignment
         * We set by default the parameters as the arguments
         * If arguments are an INT or a STRING, we define a new condition as id = the INT || STRING
         */

        $conditionsCell = false;
        if(is_int($arguments)) { $conditionsCell = $arguments; }
        if(is_string($arguments) && intval($arguments)) { $conditionsCell = intval($arguments); }
        if(is_string($arguments) && $builder['searchByProps']) { $conditionsCell = $arguments; }
        if(is_array($arguments) && count($arguments) > 0 && is_string($arguments[0]) && intval($arguments[0])) { $conditionsCell = $arguments[0]; }


        if($conditionsCell && is_string($conditionsCell) || is_int($conditionsCell)) {
            $prop = 'id';
            if ($builder['searchByProps']) {
                $property = $builder['searchByProps'];
                /** @var AbstractDbClass $object * */
                $object = new $modelFullName();
                $ObjectMap = $object->getObjectPropertiesKeyed('camel');
                if (isset($ObjectMap[$property])) { $prop = $ObjectMap[$property]->name; }
            }

            $conditions[] = sprintf('%s = "%s"', $prop, $conditionsCell);

            if(is_array($arguments)) { $arguments = array_slice($arguments, 1); }
            else { $arguments = []; }
        }

        if(is_array($arguments) && $arguments && !$conditionsCell) {
            if(isset($arguments[0])) {
                if(is_array($arguments[0])) {
                    $conditions = array_merge($arguments[0], $conditions);
                    $arguments = array_slice($arguments, 1);
                } elseif(!is_array($arguments[0])) {
                    $conditions = array_merge($arguments, $conditions);
                    $arguments = array_slice($arguments, 1);
                }
            }
        }
        /*
         * ADDITIONALS PARTS
         * Important : in the arguments we should ONLY HAVE THE ADDITIONALS NOW
         * Detect and assign the additionals argument to the queryBuilder
        */

        if(is_array($arguments) && count($arguments) == 1) {
            if (!is_array($arguments[0])) {
                $arguments[0] = [$arguments[0]];
            }
            $additionals = array_merge($additionals, $arguments[0]);
        }

        if(isset($arguments['additionals'])) { $additionals = array_merge($additionals, $arguments['additionals']); }
        if(isset($arguments['add'])) { $additionals = array_merge($additionals, $arguments['add']); }

        $builder['additionals'] = $additionals;

        return $this->abstractGet($conditions, $builder);
    }
}