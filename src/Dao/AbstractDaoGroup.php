<?php
namespace Project\Dao;

use Project\Utilities\DirectoryManipulator;
use ReflectionClass;

class AbstractDaoGroup extends AbstractDao
{
    private $Daos = [];

    private function addDao($name, AbstractDao $Dao)
    {
        $this->Daos[$name] = $Dao;
    }

    public function getDao($name): AbstractDao
    {
        if(!isset($this->Daos[$name])) { return $this; }
        return $this->Daos[$name];
    }

    protected function autoloadDaos(\mysqli $Mysqli)
    {
        foreach (DirectoryManipulator::getDirectory($this->getDir()) as $DaoFile) {
            $DaoFile = str_replace('.php', '', $DaoFile);
            $completePath = sprintf('%s\\%s', __NAMESPACE__, $DaoFile);

            if(get_class($this) === $completePath) { continue; }
            if(!class_exists($completePath)) { continue; }
            if($completePath == AbstractDao::class) { continue; }

            $newDao = new $completePath($Mysqli);
            if(!$newDao instanceof AbstractDao) { continue; }

            $DaoFile = str_replace('Dao','', $DaoFile);
            $this->addDao($DaoFile, $newDao);
        }
    }

    public function findDaoFromClass($class)
    {
        $className = (new ReflectionClass($class))->getShortName();
        return $this->getDao($className);
    }
}