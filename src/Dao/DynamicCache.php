<?php

namespace Project\Dao;

/*
 * Dynamic cache is a simple static method that store AbstractDb and manuals results in it.
 * Thanks to it, we can avoid duplicate request inside the current context.
 * It's not a long-live cache method but it's still useful.
 */

use Project\Models\AbstractDbClass;
use Project\PageManager;
use Project\Utilities\SimpleDataPerfTest;

class DynamicCache {
    static private $Cache = [];
    static private $ConditionMapper = [];

    static private $AdvancedCache = [];

    static public $Count = 0;


    static public function getModelFromCacheById($ModelName, $id)
    {

        if(!isset(static::$Cache[$ModelName])) { return false; }
        if(!isset(static::$Cache[$ModelName][$id])) { return false; }

        //Load from cache suceed !
        SimpleDataPerfTest::incrementData('dynamic_cache_object', 1);
        static::$Count++;
        return static::$Cache[$ModelName][$id];
    }

    static public function getResultsFromAdvancedCache($ModelName, $Condition)
    {
        if(is_array($Condition)) { $Condition = PageManager::getDao()->buildCondition($Condition); }
        if(!isset(static::$AdvancedCache[$ModelName])) { static::$AdvancedCache[$ModelName] = []; }
        $ModelCache = static::$AdvancedCache[$ModelName];
        if(!isset($ModelCache[$Condition])) { return false; }

        SimpleDataPerfTest::incrementData('dynamic_cache_object', 1);
        static::$Count++;
        return $ModelCache[$Condition];
    }

    /**
     * @param $ModelName
     * @param AbstractDbClass $Object
     * @return bool
     */
    static public function addToDynamicCache($ModelName, $Object, $buildedCondition = '')
    {
        if(!$Object instanceof AbstractDbClass) { return false; }

        if(!isset(static::$Cache[$ModelName])) { static::$Cache[$ModelName] = []; }
        static::$Cache[$ModelName][$Object->getId()] = $Object;

        if(!$buildedCondition) { return; }

        //We know that THIS condition on THIS object will give that result !
        static::$ConditionMapper[$ModelName][$Object->getId()] = $buildedCondition;
    }

    /**
     * @param $ModelName
     * @param AbstractDbClass $Object
     * @return bool
     */
    static public function addToDynamicAdvancedCache($ModelName, $Results, $buildedCondition = '')
    {

        if(!isset(static::$Cache[$ModelName])) { static::$Cache[$ModelName] = []; }

        //We store that this EXACT condition on this Model give those results.
        static::$AdvancedCache[$ModelName][$buildedCondition] = $Results;
    }

    public static function getCacheByModel($ModelName): array
    {
        if(!isset(static::$Cache[$ModelName])) { return []; }
        return static::$Cache[$ModelName];
    }

    /**
     * @return array
     */
    public static function getCache(): array
    {
        return static::$Cache;
    }

    /**
     * @return array
     */
    public static function getAdvancedCache(): array
    {
        return static::$AdvancedCache;
    }

    /**
     * @return array
     */
    public static function getConditionMapper(): array
    {
        return static::$ConditionMapper;
    }

}