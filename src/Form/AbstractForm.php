<?php
namespace Project\Form;

use Project\Models\AbstractDbClass;

abstract class AbstractForm {
    private $schema = [];
    private $datas = [];
    private $mode = 'POST';
    private $prefix = false;

    /**
     * @return array
     */
    public function getSchema()
    {
        return $this->schema;
    }

    public function setSchema($schema)
    {
        $this->schema = $schema;
        return $this;
    }

    public function addSchema($key)
    {
        $this->schema[] = $key;
        return $this;
    }

    abstract protected function checkValidity(array $datas);

    public function bindDataSchema()
    {
        $datas = [];
        foreach ($this->getSchema() as $key) {
            if($this->getPrefix()) {
                if (!isset($_REQUEST[$this->getPrefix()][$key])) { continue; }
                $datas[$key] = $_REQUEST[$this->getPrefix()][$key];
                continue;
            }

            if (!isset($_REQUEST[$key])) { continue; }
            $datas[$key] = $_REQUEST[$key];
        }

        if(!$this->checkValidity($datas)) { return false; }
        $this->setDatas($datas);
        return true;
    }

    public function process()
    {
        $this->bindDataSchema();
        return $this->validation();
    }

    /**
     * @return array
     */
    public function getDatas(): array
    {
        return $this->datas;
    }

    public function getData($key)
    {
        if(!isset($this->getDatas()[$key])) { return false; }
        return $this->getDatas()[$key];
    }

    /**
     * @param array $datas
     */
    public function setDatas(array $datas): void
    {
        $this->datas = $datas;
    }

    abstract protected function validation($attachedObject = false);

    abstract public function validateExisting(AbstractDbClass $object);


    public function phoneField(string $phone): string
    {
        return str_replace(' ','',  str_replace('.', '', $phone ) );
    }

    /**
     *
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @return AbstractForm
     */
    public function setPrefix($prefix): AbstractForm
    {
        $this->prefix = $prefix;
        return $this;
    }

}