<?php
namespace Project\Security\Rules;

use Project\Security\ConnexionManager;
use Project\Security\Security;

abstract class AbstractRule
{
    private $id;
    private $name;

    private $result = true;
    private $heritage = [];

    abstract public function action();
    abstract public function process(): bool;

    final function startRule(): bool
    {
        foreach ($this->getHeritage() as $heritage) {
            $heritageRule = ConnexionManager::getSecurity()->getRule($heritage);
            if(!$heritageRule->startRule()) { return false; }
        }
        return $this->process() === $this->getResult();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return AbstractRule
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return AbstractRule
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeritage(): array
    {
        return $this->heritage;
    }

    /**
     * @param mixed $heritage
     * @return AbstractRule
     */
    public function addHeritage($heritage): AbstractRule
    {
        if(is_array($heritage)) {
            $this->heritage = array_merge($heritage, $this->heritage);
        } else {
            $this->heritage[] = $heritage;
        }
        return $this;
    }
    /**
     * @param array $heritage
     * @return AbstractRule
     */
    public function setHeritage(array $heritage): AbstractRule
    {
        $this->heritage = $heritage;
        return $this;
    }

    /**
     * @return bool
     */
    public function getResult(): bool
    {
        return $this->result;
    }

    /**
     * @param bool $result
     * @return AbstractRule
     */
    public function setResult(bool $result): AbstractRule
    {
        $this->result = $result;
        return $this;
    }


}