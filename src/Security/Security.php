<?php
namespace Project\Security;

use Project\Security\ConnexionManager;
use Project\PageManager;
use Project\Security\Route;
use Project\Security\Rules\AbstractRule;
use Project\Utilities\SimpleDataPerfTest;

class Security extends AbstractSecurity
{
    /*
     * Now in the construct, you are able to store ONLY the rules with property modified.
     * Others are get automaticly.
     *
     */
    public function __construct()
    {
        //Add your custom rules here
    }
}