<?php
namespace Project\Security;

use Core\CorePageManager;
use Project\PageManager;
use Project\Security\Rules\AbstractRule;
use Project\Security\Rules\EmptyRule;
use Project\Utilities\SimpleDataPerfTest;

class AbstractSecurity
{
    protected $rules = [];

    /*
 * Get a rule defined in the Security Process
 * + Rules finded automaticaly are now stored: We gain some ultra minor perf, but better
 */
    public function getRule($rule): AbstractRule
    {
        //First step: We try to get a manually defined rule
        if(isset($this->rules[$rule])) {
            SimpleDataPerfTest::incrementData('rule_cache', 1);
            return $this->rules[$rule];
        }

        //If this rule doesn't exist, we'll try to find if the rule exists somewhere
        $ruleNameParsed = str_replace('_',' ', $rule);
        $ruleNameParsed = strtolower($ruleNameParsed);
        $ruleNameParsed = ucwords($ruleNameParsed);
        $ruleNameParsed = str_replace(' ', '', $ruleNameParsed);

        $completeNameSpace = sprintf('Project\\Security\\Rules\\%s', $ruleNameParsed);

        if(class_exists($completeNameSpace)) {
            $class = new $completeNameSpace();
            if($class instanceof AbstractRule) {
                $this->rules[$rule] = $class;
                return $class;
            }
        }

        //Finally, if this rule doesn't exist after all, return the Empty Rule to prevent error
        return new EmptyRule();
    }

    public function checkSecurity($securityRules): bool
    {
        if(!is_array($securityRules)) { $securityRules = [$securityRules]; }

        foreach ($securityRules as $security) {
            $rule = $this->getRule($security);
            if(!$rule->startRule()) { return false; }
        }
        return true;
    }

    public function checkCurrentRouteSecurity()
    {
        $Route = PageManager::getRouter()->getCurrentRoute();

        foreach ($Route->getSecurityLevel() as $security) {
            $rule = $this->getRule($security);
            if ($rule->startRule()) { continue; }

            ConnexionManager::addFlashSessionMessage('warning', 'Accès refusé.', 1);

            return PageManager::goToLastPage();
        }
        return true;
    }

    public function checkRouteSecurity($Route): bool
    {
        if(!$Route instanceof Route) { $Route = PageManager::getRouter()->getRoute($Route); }

        foreach ($Route->getSecurityLevel() as $security) {
            $rule = $this->getRule($security);
            if(!$rule->startRule()) { return false; }
        }

        return true;
    }
}