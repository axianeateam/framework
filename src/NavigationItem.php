<?php
namespace Project;

class NavigationItem {
    private $page;
    private $additional;
    private $arg;
    private $icon;
    private $title;
    private $titleClass;

    /**
     * NavigationItem constructor.
     * @param $page
     * @param $icon
     * @param $title
     * @param bool $additional
     * @param bool $arg
     */
    public function __construct($page, $icon, $title,  $additional = false, $arg = false)
    {
        $this->page = $page;

        $this->icon = $icon;
        $this->title = $title;

        $this->additional = $additional;
        $this->arg = $arg;
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     * @return NavigationItem
     */
    public function setPage($page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * @param mixed $additional
     * @return NavigationItem
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getArg()
    {
        return $this->arg;
    }

    /**
     * @param mixed $arg
     * @return NavigationItem
     */
    public function setArg($arg)
    {
        $this->arg = $arg;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     * @return NavigationItem
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return NavigationItem
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
}