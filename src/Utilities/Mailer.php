<?php
namespace Project\Utilities;
use PHPMailer\PHPMailer\PHPMailer;
use Project\PageManager;

class Mailer {
    private $to = [];
    private $subject;
    private $headers = [];
    private $message;
    private $isActivate = true;
    private $hasNotice = true;

    public function sendMail()
    {
        /**
         * - Disable Mail if Mailer is desactivate.
         * Cool for Testing
         * Users should be able to desactivate it to legal purposes
         */
        if(!$this->isActivate()) { return false; }

        $mail = new PHPMailer;

        $mail->isSMTP();
        $mail->isHTML(true);
        $mail->CharSet = 'utf-8';
        $mail->SMTPDebug = 0;
        $mail->Host = getenv('MAILER_HOST');
        $mail->Port = getenv('MAILER_PORT');
        $mail->SMTPAuth = true;
        $mail->Username = getenv('MAILER_USERNAME');
        $mail->Password = getenv('MAILER_PASSWORD');
        $mail->setFrom(getenv('MAILER_FROM'), PageManager::getSiteName());
        $mail->addReplyTo(getenv('MAILER_REPLY_TO'), PageManager::getSiteName());
        $mail->addAddress($this->getTo(), getenv('MAILER_REPLY_NAME'));
        $mail->Subject = $this->getSubject();
        $mail->msgHTML($this->getMessage());

        $result = $mail->send();
        if(!$this->getHasNotice()) { return; }

        if($result) {
            ConnexionManager::addFlashSessionMessage('success', "Mail envoyé avec succès.");
            return;
        }

        ConnexionManager::addFlashSessionMessage('warning', "Un problème est survenu lors de l'envoi du mail.");
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    public function getReceivers()
    {
        return implode(', ', $this->getTo());
    }

    /**
     * @param mixed $to
     * @return Mailer
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    public function addReceiver($receiver)
    {
        $this->to[] = $receiver;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     * @return Mailer
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param mixed $headers
     * @return Mailer
     */
    public function setHeaders($headers): Mailer
    {
        $this->headers = $headers;
        return $this;
    }

    public function addHeader($key, $message): Mailer
    {
        $this->headers[$key] = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    public function getMessageWrapped(): string
    {
        return wordwrap($this->getMessage(), 70, "\r\n");
    }

    /**
     * @param mixed $message
     * @return Mailer
     */
    public function setMessage($message): Mailer
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHasNotice(): bool
    {
        return $this->hasNotice;
    }

    /**
     * @param bool $hasNotice
     * @return Mailer
     */
    public function setHasNotice(bool $hasNotice): Mailer
    {
        $this->hasNotice = $hasNotice;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActivate(): bool
    {
        return $this->isActivate;
    }

    /**
     * @param bool $isActivate
     * @return Mailer
     */
    public function setIsActivate(bool $isActivate): Mailer
    {
        $this->isActivate = $isActivate;
        return $this;
    }
}