<?php


namespace Project\Utilities;

class SimpleDataPerfTest
{
   static protected $datas;
   /*********************/

    /**
     * @return array
     */
    public static function getDatas(): array
    {
        return static::$datas;
    }

    public static function getData($id): array
    {
        if(!isset(static::$datas[$id])) {
            return [
                'name' => '',
                'value' => 0
            ];
        }
        return static::$datas[$id];
    }

    public static function addData($id, $name, $value = 0)
    {
        static::$datas[$id] = [
            'name' => $name,
            'value' => $value
        ];
    }

    public static function incrementData($id, $amount = 1, $name = '') {
        if(!isset(static::$datas[$id])) {
            static::addData($id, $name, $amount);
            return;
        }
        static::$datas[$id]['value'] += $amount;
    }

    /**
     * @param array $datas
     */
    public static function setQueries(array $datas): void
    {
        static::$datas = $datas;
    }
}
