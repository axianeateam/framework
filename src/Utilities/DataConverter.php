<?php
namespace Project\Utilities;

class DataConverter {
    static public function mail($mail)
    {
        if(!$mail) { return $mail; }

        $mail = strtolower($mail);
        $startBeforeAt = preg_split('/@([A-z\.\-\_]+)/', $mail)[0];
        $startBeforeAt = str_replace('.', '',$startBeforeAt);
        $startBeforeAt = str_replace('-', '',$startBeforeAt);
        $startBeforeAt = str_replace('_', '',$startBeforeAt);
        $startBeforeAt = str_replace(' ', '',$startBeforeAt);
        $end = preg_split('/([A-z\.\-\_]+)@/', $mail)[1];
        $cleanMail  = sprintf('%s@%s', $startBeforeAt, $end);
        return $cleanMail;
    }

    static public function phone($phone)
    {
        if(!$phone) { return $phone; }

        $pattern = '/[0-9]{2}/';
        $replacement = '${1}.';
        preg_match_all($pattern, $phone, $matches);
        return implode('.', $matches[0]);
    }

    static public function amount($amount,bool $attachEuroSymbol = false)
    {
        $formated = number_format($amount, 2, '.', ' ');
        if($attachEuroSymbol) { $formated .= '€'; }

        return $formated;
    }

    static public function wordingPlurial($quantity, $word, $plurial = 's')
    {
        if($quantity == 0 || $quantity == 1) { return $word; }
        return $word.$plurial;
    }


    static public function toBoolean($value)
    {
        if($value === 0) { return false; }
        if($value === false) { return false; }
        if($value === 'false') { return false; }
        if(!$value) { return false; }

        if($value === 1) { return true; }
        if($value === true) { return true; }
        if($value === 'true') { return true; }
        if($value) { return true; }
    }
}