<?php

namespace Project\Utilities;


use Project\PageManager;
use Project\Security\ConnexionManager;

class ErrorCatcher
{
    static private $errors = [];

    public static function addError($error)
    {
        static::$errors[] = $error;

        $env = getenv('ENV_MODE');
        if( $env != 'prod' && $env != 'master' && $env ) { return true; }


        $SlackNotification = new SlackNotification();
        $SlackNotification->setApiUrl('chat.postMessage');
        $SlackNotification->setChannel(getenv('SLACK_ERROR_NOTIFIER_CHANNEL'));

        $userName = PageManager::getSiteName()." : Erreur Repérée";
        if(method_exists(ConnexionManager::class, 'getAccount')) {
            $Account = ConnexionManager::getAccount();
            if($Account && is_object($Account) && method_exists($Account, 'getName')) {
                $userName .= sprintf('par %s %s', $Account->getName(), $Account->getFirstName());
            }
        }
        $userName .= ' !';

        $SlackNotification->setUsername($userName);

        $text = "";
        $text .= ErrorCatcher::generateClassCallErrorMessage(count(static::$errors)-1)."\r\n\r\n";
        if(isset($error['errorObject'])) {
            $text .= ErrorCatcher::getErrorTraceMessage($error, 'last');
        }

        $SlackNotification->setText($text);
        $SlackNotification->send();
    }


    public static function addClassErrorMessage($class, $method, $args = [], $error = [])
    {
        //If the arg is a string, add "" between for the rendering message.
        foreach ($args as $i=> $arg) {
            if(is_string($arg)) { $args[$i] = sprintf('"%s"', $arg); }
        }

        static::addError([
            'class' => $class,
            'method' => $method,
            'args' => $args,
            'errorObject' => $error
        ]);
    }

    public static function generateClassCallErrorMessage($n)
    {
        $error = static::getError($n);
        if(!$error) { return false; }

        return sprintf(
            "%s->%s(%s) n'existe pas.",
            $error['class'],
            $error['method'],
            implode(',', $error['args'])
        );
    }
    public static function getErrorTraceMessage($error, $n)
    {
        /** @var \Error $errorObject */
        $errorObject = $error['errorObject'];

        switch($n) {
            case 'last': $n = 0; break;
            case 'first': $n = count($errorObject->getTrace()) - 1; break;
        }

        if(!isset($errorObject->getTrace()[$n])) { return ''; }

        $errorTraced = $errorObject->getTrace()[$n];

        return sprintf('%s on line %d. <br>Origin: %s%s%s()',
            $errorTraced['file'],
            $errorTraced['line'],
            $errorTraced['class'],
            $errorTraced['type'],
            $errorTraced['function']
        );
    }

    /**
     * @return array
     */
    public static function getErrors(): array
    {
        return self::$errors;
    }

    public static function getError($n)
    {
        if(!isset(static::$errors[$n])) { return false; }
        return static::$errors[$n];
    }

    /**
     * @param array $errors
     */
    public static function setErrors(array $errors): void
    {
        self::$errors = $errors;
    }
}