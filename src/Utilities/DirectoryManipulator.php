<?php


namespace Project\Utilities;

class DirectoryManipulator
{
    /*
     *  We slice the 2 first entries of the dir. Because on linux the 2 first entries are '.' and '..'
     */
    static public function getDirectory($directoryPath): array
    {
        if(!is_dir($directoryPath)) { return []; }
        return array_slice(scandir($directoryPath), 2);
    }
}