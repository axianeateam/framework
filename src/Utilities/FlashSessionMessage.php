<?php
namespace Project\Utilities;

class FlashSessionMessage {
    public $type;
    public $message;
    public $report = 0;

    public function generateFlashMessage(): string
    {
        return sprintf('
            <div class="alert alert-dismissible fade show alert-%s" role="alert">
                %s
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>',
            $this->type,
            $this->message
        );
    }
}