<?php
namespace Project\Core;

use Project\Models\AbstractDbClass;
use Project\Utilities\ErrorCatcher;
use ReflectionClass;

class BaseObject {

    private $protectedProperties = [];
    private $autoload_objects = true;


    public function dynamicGetterByProp($propName)
    {
        $format = str_replace('_', ' ', $propName);
        $format = ucwords($format);
        $cleanProp = str_replace(' ','', $format);

        $propName = 'get'.$cleanProp;

        if(method_exists($this, $propName)) {
            $result = $this->$propName();
            if(is_object($result)) { return $result->getId(); }
            return $result;
        }
        return '';
    }

    /**
     * Get the properties of an object. ReflectionClass return an array like [ => Object{name: @name, objectOrigin: @Object} ]
     * @return array
     */
    public static function getObjectProperties(): array
    {
        $reflect = new ReflectionClass(static::class);
        return $reflect->getProperties();
    }

    /**
     * @param string $caseType (can be camel or snake)
     * @return array
     */
    public static function getObjectPropertiesKeyed($caseType = 'snake')
    {
        $reflected = static::getObjectProperties();
        $properties = [];
        foreach($reflected as $reflect) {
            //default is snake
            $format = $reflect->name;
            if($caseType == 'camel') {
                $format = str_replace('_', ' ', $reflect->name);
                $format = ucwords($format);
                $format = str_replace(' ','', $format);
            }
            $properties[$format] = $reflect;
        }
        return $properties;
    }

    public function getClassShortName()
    {
        $reflect = new ReflectionClass($this);
        return $reflect->getShortName();
    }

    public function getClassCompleteName()
    {
        $reflect = new ReflectionClass($this);
        return $reflect->getName();
    }

    /**
     * Assign an array [@property => @value] to an object.
     * @param array $array
     * @param string $prefix
     * @return int
     */
    public function bindParametersToObject(array $array = [], $prefix = '')
    {
        $updatedParameters = 0;

        foreach ( $array as $property => $data) {
            $cleanProp = str_replace('_','', ucfirst(str_replace($prefix,'', $property)));
            $propName = sprintf('set%s', $cleanProp);
            if(!method_exists($this, $propName)) { continue; }
            $updatedParameters++;
            $this->{$propName}($data);
        }
        return $updatedParameters;
    }

    /**
     * Convert an object to an array like [@property => @value, ...]
     * Is Recursive, if Object has an Object in prop, convert it too.
     * @param string $prefix
     * @param bool $autoload
     * @return array
     */
    public function objectToArray($prefix = '', $autoload = true): array
    {
        $result = [];

        foreach (static::getObjectProperties() as $objectProperty) {
            $format = str_replace('_', ' ', $objectProperty->name);
            $format = ucwords($format);
            $cleanProp = str_replace(' ','', $format);

            if($this->isPropertyProtected($cleanProp)) { continue; }

            $propName = sprintf('get%s', $cleanProp);

            if(!method_exists($this, $propName)) { continue; }

            if(is_object($this->{$propName}())) {
                if($autoload) {
                    $result[$objectProperty->name] = $this->{$propName}()->objectToArray('', false);
                } else {
                    $result[$objectProperty->name] = $this->{$propName}()->getId();
                }

                continue;
            }

            $result[$objectProperty->name] = $this->{$propName}();
        }

        return $result;
    }

    static public function MultipleObjectsToArray($objectArray = []): array
    {
        $response = [];
        foreach ($objectArray as $object) { $response[] = $object->objectToArray(); }
        return $response;
    }

    /**
     * @return bool
     */
    public function isAutoloadObjects(): bool
    {
        return $this->autoload_objects;
    }

    /**
     * @param bool $autoload_objects
     * @return BaseObject
     */
    public function setAutoloadObjects(bool $autoload_objects): BaseObject
    {
        $this->autoload_objects = $autoload_objects;
        return $this;
    }


    /**
     * @return array
     */
    public function getProtectedProperties(): array
    {
        return $this->protectedProperties;
    }

    public function isPropertyProtected($property): bool
    {
        if(isset($this->protectedProperties[$property])) { return true; }
        return false;
    }

    /**
     * @param array $protectedProperties
     * @return AbstractDbClass
     */
    public function setProtectedProperties(array $protectedProperties): AbstractDbClass
    {
        $this->protectedProperties = $protectedProperties;
        return $this;
    }

    protected function addProtectedProperties($propertyName) {
        $this->protectedProperties[$propertyName] = true;
    }

    public function __call($method, $arguments)
    {
        $error = new \Error();
        ErrorCatcher::addClassErrorMessage($this->getClassShortName(), $method, $arguments, $error);

    }
}