<?php
namespace Project\Core;

use Project\Admin\AbstractController;
use Project\Utilities\DirectoryManipulator;

abstract class CoreControllerManager
{
    protected $controllers = [];

    public function addController($ControllerClass, $key, $arguments = [], $route = ''): bool
    {
        if(!class_exists($ControllerClass)) { return false; }
        /* @var CoreController $newController */
        $newController = new $ControllerClass();
        $newController->setKey($key);
        $newController->setArguments($arguments);
        $newController->setRoute($route);

        if(isset($this->controllers[$key])) { return false; }

        $this->controllers[$key] = $newController;

        return true;
    }

    public function addTrinityController($ControllerClass, $key, $arguments = [], $route = ''): bool
    {
        $this->addController($ControllerClass, $key, $arguments, $route);
        $this->controllers[$key.'/ajout'] = $key;
        $this->controllers[$key.'/edition'] = $key;
        $this->controllers[$key.'/supprimer'] = $key;
        return true;
    }


    /**
     * getProcess works in 2 steps
     * Step 1: We try to find the Controller defined in $this->controllers (added in __construct). If the Controller exists,
     * process()
     * Step 2: We try to find automatically a Controller who has the same name as the key. if controller exists, process()
     *
     * @param $key
     * @return bool|void
     */
    public function getProcess($key)
    {
        if(isset($this->controllers[$key])) {

            if (!$this->controllers[$key] instanceof AbstractController && is_string($this->controllers[$key])) {
                $this->getProcess($this->controllers[$key]);
                return;
            }

            $this->controllers[$key]->process();
            return;
        }

        $parsedKey = str_replace('-',' ',$key);
        $parsedKey = str_replace('/',' ',$parsedKey);
        $parsedKey = str_replace('_',' ',$parsedKey);
        $parsedKey = ucwords($parsedKey);
        $parsedKey = str_replace(' ','',$parsedKey);

        $baseNameSpace = 'Project\\Controllers';
        $basePath = '../src/Controllers';

        $baseDirectory = DirectoryManipulator::getDirectory($basePath);

        foreach ($baseDirectory as $subDir) {
            $context = $basePath.'/'.$subDir;
            $ScopedNameSpace = sprintf('\%s\%s\%s', $baseNameSpace, $subDir, $parsedKey);

            $AlternativeNameSpace = sprintf('\\%s\\%s\\%s', $baseNameSpace, $subDir, $subDir);
            $CleanAlternativeNameSpace = str_replace($AlternativeNameSpace, '', $ScopedNameSpace);
            $CleanAlternativeNameSpace = sprintf('\\%s\\%s\\%s', $baseNameSpace, $subDir, $CleanAlternativeNameSpace);

            if(!class_exists($CleanAlternativeNameSpace) && class_exists($CleanAlternativeNameSpace.'Controller')) {
                $CleanAlternativeNameSpace .= 'Controller';
            }

            //We try both syntaxes ! This is the flexiest option for developpers
            if(!class_exists($ScopedNameSpace) && class_exists($ScopedNameSpace.'Controller')) {
                $ScopedNameSpace .= 'Controller';
            }

            $class = false;
            if(class_exists($ScopedNameSpace)) { $class = new $ScopedNameSpace(); }
            else if(class_exists($CleanAlternativeNameSpace)) { $class = new $CleanAlternativeNameSpace(); }
            if($class && $class instanceof CoreController) {
                $class->process();
                return;
            }
        }
    }
}