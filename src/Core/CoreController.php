<?php
namespace Project\Core;

use Project\Security\ConnexionManager;
use Project\PageManager;

abstract class CoreController
{
    /* must be unique */
    private $key = null;

    private $arguments = [];
    protected $parameters = [];
    private $route = null;

    protected abstract function action();

    protected function error()
    {

    }

    /*
   * Tests here
   */
    public function process() {
        $params = [];
        foreach ($this->getArguments() as $argument) {
            $req = ConnexionManager::getRequest($argument);
            if(!$req) {
                $this->error();
                return;
            }
            $this->parameters[$argument] = $req;
        }

        $this->action();
    }

    /**
     * @return null
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param null $key
     * @return CoreController
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     * @return CoreController
     */
    public function setArguments(array $arguments): CoreController
    {
        $this->arguments = $arguments;
        return $this;
    }

    /**
     * @return null
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param null $route
     * @return CoreController
     */
    public function setRoute($route)
    {
        $this->route = $route;
        return $this;
    }


}