<?php
namespace Core;

use Project\Dao\AbstractDao;
use Project\Controllers\ControllerManager;
use Project\PageManager;
use Project\Security\ConnexionManager;
use Project\Security\Route;
use Project\Security\Router;
use Project\Utilities\DataConverter;

abstract class CorePageManager
{
    static protected $Router = null;
    static protected $Dao = null;
    static protected $Controller = null;

    static function getMainDao(): AbstractDao
    {
        return static::$Dao;
    }
    static function getDao(): AbstractDao
    {
        return static::$Dao;
    }

    /**
     * @return Router
     */
    static public function getRouter(): Router
    {
        return static::$Router;
    }

    static function getControllerManager(): ControllerManager
    {
        if(!static::$Controller) {
            static::$Controller = new ControllerManager();
        }
        return static::$Controller;
    }


    static function getAsset($type, $name)
    {
        return sprintf('%s/assets/%s/%s', self::getPublicPath(), $type, $name);
    }

    static function getPublicPath()
    {
        $replacement = sprintf('%s/public', static::getProjectAbsoluteRoot());
        return preg_replace(sprintf('/.+%s\/(.+)/', static::getBaseRoot()), $replacement, __DIR__);
    }

    static function getHomePage(): string
    {
        return '/';
    }

    static function goHome()
    {
        if(ConnexionManager::isConnected()) {
            static::goTo(static::getHomePage());
            return;
        }
        static::goTo('login');
    }

    static public function goToLastPage()
    {
        /**@var Route $lastPage */
        $lastPage = ConnexionManager::getFromSession('last_route');
        if(!$lastPage) { static::goHome(); return false; }
        if(!ConnexionManager::isConnected()) { static::goHome(); return false; }

        PageManager::goToRoute($lastPage);
        return true;
    }

    static public function updateLastRoute()
    {
        $currentRoute = ConnexionManager::getFromSession('current_route');
        $lastRoute = ConnexionManager::getFromSession('last_route');

        if($currentRoute == $lastRoute) { return false; }
        if($currentRoute && $currentRoute->isComplete()) {
            ConnexionManager::setToSession('last_route', $currentRoute);
        }
    }

    static function getController()
    {
        $devDebug = DataConverter::toBoolean(ConnexionManager::getRequest('dev_debug_mode'));
        if(isset($_REQUEST['dev_debug_mode'])) {
            ConnexionManager::setToSession('dev_debug_mode', $devDebug);
        }


        $page = static::getFullPage();
        $route = static::getRouter()->getRoute($page);

        //If we're not allowed to be at this Route, we block the process to prevent controller loading
        if(!ConnexionManager::getSecurity()->checkRouteSecurity($route)) { return false; }


        static::getRouter()->bindRouteParameters($route);

        if($route->isComplete()) {
            ConnexionManager::setToSession('current_route', $route);
        }

        static::getControllerManager()->getProcess($route->getFile());

        $args = $route->getArguments();
        if(isset($args['isApi']) && $args['isApi']) { exit; }
    }

    static public function router($page, $convert = false)
    {
        $router = static::getRouter();
        $route = $router->getRoute($page);

        //Debug mode, kill process when route not find.
        if(!$route->getFile()) {
            //dump('Non trouvé : ',$page);die;
            static::goTo('404');
        }


        if($page === $route->getFile()) { return $route->getSlug(); }
        if($page === $route->getSlug()) { return $route->getFile(); }


        //No route is find, let's return home
        return static::getRouter()->getRoute(static::getHomePage())->getSlug();
    }

    /**
     * Load a page (current without argument or forced with $page defined)
     * @see PageManager::router()
     * @see PageManager::getPage()
     *
     * @param mixed $page
     * @param array $additionalExpose
     * Use to expose variable internally, all of the variables in $additionalExpose will be exposed like :
     * $key = value => Important : that's the $key that become the var name. ['test' => 'a'] -> $test === a
     *
     * @param bool $keepExposed
     * If $keepExposed is true, the loadPage call will not erase the variables exposed after using it.
     * Be careful with this use. It's useful if your mainpage has subpages and when you want to keep your datas available
     * in those subpages automatically. Use it only if it needed.
     *
     * @param bool $isSubpage
     * If $isSubpage is defined to true, loadPage will get parent()/$subPage to find the route instead of $page
     */
    static function loadPage($page = false,array $additionalExpose = [] ,$keepExposed = false, $isSubpage = false):void
    {
        if(!$page) { $page = self::getPage(); }
        if($isSubpage) {
            $page = sprintf("%s/%s",self::getPage(), ConnexionManager::getRequest('subpage'));
        }

        $route = static::getRouter()->getRoute($page);

        if(!$route || !$route->isComplete() ) {
            require_once './page/errors/404.php';
            return;
        }

        $fileRoot = sprintf('./page/%s.php', $route->getFile());

        if(!file_exists($fileRoot)) { return; }

        /*
         *  Expose the datas stored in models/controllers as local variables.
         *  Use by your renderer templates. $key become the $var_name.
         *  The variables are available only at this scope (because the require is in this method
         */

        $varExposed = ConnexionManager::getFromSession('var_exposed');
        if(!is_array($varExposed)) { $varExposed = []; }

        foreach (array_merge($varExposed, $additionalExpose) as $key => $var) {
            if(!$key) { continue; }
            ${$key} = $var;
        }

        require_once( $fileRoot );

        ConnexionManager::setToSession('var_exposed', []);
    }

    static public function getFullPage()
    {
        $basePath = static::getPage();

        $mode = ConnexionManager::getRequest('mode');
        if($mode) {
            $basePath .= '/'.$mode;
        }

        return $basePath;
    }

    static public function getPage()
    {
        if(!isset($_REQUEST['page'])) { return static::getHomePage();  }
        return $_REQUEST['page'];
    }

    static public function reload()
    {
        static::goTo(static::getPage());
    }

    static public function getProjectAbsoluteRoot()
    {
        $path = sprintf('//%s',$_SERVER['HTTP_HOST']);
        $baseRoot = static::getBaseRoot();

        if($baseRoot) { $path .= sprintf('/%s', $baseRoot); }

        return $path;
    }

    static function getBaseRoot()
    {
        return '';
    }

    /**
     * @param Route $Route
     * @param bool $arg
     * @param bool $completeUrl
     * @return string
     */
    static function getRouteCompleteUrl($Route, $arg = false, $completeUrl = true)
    {
        if(!$arg) {
            $arg = $Route->extractParameters();
        }

        $pagePath  = $Route->getSlug($arg);
        if(!$completeUrl) { return $pagePath; }

        $path = static::getProjectAbsoluteRoot();
        return sprintf('%s/%s', $path, $pagePath);
    }

    static function getPageUrl($page, $additional = false, $arg = false, $completeUrl = true)
    {
        if($additional) { $page .= '/'. $additional; }
        $route = static::getRouter()->getRoute($page);

        return static::getRouteCompleteUrl($route, $arg, $completeUrl);

    }


    static public function getLastPageUrl()
    {
        /**@var Route $lastPage */
        $lastRoute = ConnexionManager::getFromSession('last_route');

        if(!$lastRoute) { $lastRoute  = static::getRouter()->getRoute(static::getHomePage()); }

        return static::getRouteCompleteUrl($lastRoute);
    }


    /**
     *
     * @param array $parameters
     * @return bool
     * @var Route $Route
     */
    static public function goToRoute($Route, $parameters = [])
    {
        if(!$parameters) { $parameters = $Route->extractParameters(); }

        $page = $Route->getSlug($parameters);

        header(sprintf('Location: %s', static::getPageUrl($page, '', $parameters)));
        return true;
    }


    static function goTo($page, $parameters= '', $args = [])
    {
        $fullPage = $page;
        if($parameters) { $fullPage .= sprintf('/%s', $parameters); }

        $Route = static::getRouter()->getRoute($fullPage);

        if(!$parameters) { $parameters = $Route->extractParameters(); }
        if($args && !is_array($args)) { $args = [$args]; }

        static::goToRoute($Route, $args);
    }

    static public function expose($variables = [])
    {
        $var_exposed = ConnexionManager::getFromSession('var_exposed');
        if(!$var_exposed)
        { $var_exposed = []; }

        ConnexionManager::setToSession('var_exposed', array_merge($var_exposed, $variables));
    }
}