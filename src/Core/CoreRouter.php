<?php
namespace Core;


use Project\PageManager;
use Project\Security\Route;

abstract class CoreRouter
{
    protected $fileReferences = [];
    protected $slugReferences = [];

    public function __construct()
    {
        $this->initializeRoutes();
    }

    abstract protected function initializeRoutes();


    public function bindRouteParameters(Route $route): bool
    {
        $parameters = $route->extractParameters();
        if(!$parameters) { return false; }
        foreach ($parameters as $key => $parameter) { $_REQUEST[$key] = $parameter; }

        return true;
    }

    /**
     * Work in 2 steps
     * Step 1 :
     * We try a basic research like $path is a key of Router->fileReferences || Router->slugRerences.
     * If there's no Route defined, go to Step 2 =>
     * Step 2:
     * Loop on slugReferences. Test each key as a regex. If regex match $path, return the current Route.
     *
     * If Step 1 and Step 2 failed, return empty Route Object
     * => Maybe return 404 : Route not found ?
     *
     * @param $path
     * @return Route
     */
    public function getRoute($path): Route
    {
        /*
         * Step 1 : Basic search
         */
        if(isset($this->slugReferences[$path])) {
            return $this->slugReferences[$path];
        }
        if(isset($this->fileReferences[$path])) {
            return $this->fileReferences[$path];
        }

        /*
         * Step 2 : Complexe research
         */

        /** @var Route $route */
        foreach ($this->slugReferences as $key => $route) {
            $regex = preg_match(sprintf('@^%s$@', $route->getRegexSlug()), $path);
            if(!$regex) { continue; }

            return $route;
        }

        foreach ($this->fileReferences as $key => $route) {
            $regex = preg_match(sprintf('@^%s$@', $route->getFile()), $path);
            if(!$regex) { continue; }

            return $route;
        }

        return new Route();
    }

    public function getCurrentRoute(): Route
    {
        return $this->getRoute(PageManager::getPage());
    }

    protected function addRoute($file, $slug, $securityLevel = 1, $arguments = [])
    {
        $Route = new Route();
        $Route
            ->setFile($file)
            ->setSlug($slug)
            ->setSlug($Route->getRegexSlug())
            ->setSecurityLevel($securityLevel)
            ->setArguments($arguments);

        $path = $Route->getFile();
        if(isset($arguments['mode'])) {
            $path = $Route->getFile().'/'.$arguments['mode'];
        }

        $this->fileReferences[$path] = $Route;
        $this->slugReferences[$Route->getSlug()] = $this->fileReferences[$path];
    }

    protected function addTrinityRoute($file, $slug, $securityLevel = 1, $subPathAdditionalSecurity = '', $arguments = [])
    {
        //If no specific rule for subpaths, apply the core one
        if(!$subPathAdditionalSecurity) { $subPathAdditionalSecurity = $securityLevel; }

        $this->addRoute($file, $slug, $securityLevel, []);
        $this->addRoute($file, $slug.'/ajout', $subPathAdditionalSecurity,  ['mode' => 'add']);
        $this->addRoute($file, $slug.'/edition/&1', $subPathAdditionalSecurity, ['id' => '([0-9]+)', 'mode' => 'edition']);
        $this->addRoute($file, $slug.'/supprimer/&1', $subPathAdditionalSecurity,  ['id' => '([0-9]+)', 'mode' => 'remove']);
    }
}