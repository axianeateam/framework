<?php
namespace Project\Templates;

use Project\Models\AbstractDbClass;
use Project\PageManager;
use Project\Security\ConnexionManager;

/**
 * $schemaElement =  [  name, required, type, class  ]
 */

class AdvancedFormTemplate extends AbstractTemplate {

    protected static function renderInput($schemaElement, $params = [])
    {
        $default = '';
        if(isset($schemaElement['params']['default'])) { $default = $schemaElement['params']['default']; }

        $schemaAttributes = [];
        if(isset($schemaElement['params']['attributes'])) { $schemaAttributes = $schemaElement['params']['attributes']; }

        $sectionClasses = "form-group mb-0";
        if(isset($params['sectionClasses'])) {
            $sectionClasses = $params['sectionClasses'];
            if(!$params['sectionClasses']) { $sectionClasses = ''; }
        }
        ?>
        <div class="<?= $sectionClasses; ?>">
            <?php if($schemaElement['params']['label'] && !isset($params['nolabel'])): ?>
                <label class="bmd-label-floating" for="<?=$schemaElement['completeName']; ?>"><?= $schemaElement['params']['label'];?></label>
            <?php endif; ?>

            <input
                    type="<?=$schemaElement['type']; ?>"
                    id="<?=$schemaElement['completeName']; ?>"
                    name="<?=$schemaElement['completeName']; ?>"
                    class="form-control <?= $schemaElement['params']['classes']; ?>"
                    value="<?= $schemaElement['value'] ?: $default; ?>"
                    <?php foreach ($schemaAttributes as $key => $attribute): ?>
                        <?=$key;?>="<?= $attribute; ?>"
                    <?php endforeach; ?>
                <?= static::renderCondition($schemaElement['params']['required'], 'required="required"'); ?>
            >
        </div>
        <?php
    }

    protected static function renderSelectEnumOptions($schemaElement)
    {
        foreach ($schemaElement['params']['selectEnum'] as $key => $option): ?>
            <option value="<?= $key ?>" <?= static::renderCondition($schemaElement['value'] != '' && $key == $schemaElement['value'], 'selected="selected"')?>>
                <?= $option['name']; ?>
            </option>
        <?php endforeach;
    }


    protected static function renderSelectModelOptions($schemaElement)
    {
        $Models = [];
        $dao = PageManager::getDao()->findDaoFromClass($schemaElement['params']['Model']);

        $internalCondition = [];
        if(isset($schemaElement['params']['modelValues']) && $schemaElement['params']['modelValues']) {
            if(!is_array($schemaElement['params']['modelValues'])) {
                $schemaElement['params']['modelValues'] = [$schemaElement['params']['modelValues']];
            }
            $internalCondition[] = sprintf('id IN (%s)', implode(',', $schemaElement['params']['modelValues']));
        }

        $Models = $dao->{'get'.$dao->getMultipleName()}($internalCondition);
        if(!isset($schemaElement['params']['modelName'])) { $schemaElement['params']['modelName'] = 'getId'; }

        foreach ($Models as $Model): ?>
            <option value="<?= $Model->getId(); ?>" <?= static::renderCondition($schemaElement['value'] != '' && $Model->getId() == $schemaElement['value'], 'selected="selected"')?>>
                <?= $Model->{$schemaElement['params']['modelName']}(); ?>
            </option>
        <?php endforeach;
    }

    protected static function renderSelect($schemaElement, $params = [])
    {
        $sectionClasses = "form-group mb-0";
        if(isset($params['sectionClasses'])) {
            $sectionClasses = $params['sectionClasses'];
            if(!$params['sectionClasses']) { $sectionClasses = ''; }
        }
        ?>
        <div class="<?= $sectionClasses; ?>">
            <?php if($schemaElement['params']['label'] && !isset($params['nolabel'])): ?>
                <label class="bmd-label-floating" for="<?=$schemaElement['completeName']; ?>"><?= $schemaElement['params']['label'];?></label>
            <?php endif; ?>
            <select
                    id="<?=$schemaElement['completeName']; ?>"
                    name="<?=$schemaElement['completeName']; ?>"
                    class="form-control <?= $schemaElement['params']['classes']; ?>"
                <?= static::renderCondition($schemaElement['params']['required'], 'required="required"'); ?>
            >
                <option value="" selected>
                    <?php if(!isset($params['nodefaultoption'])):  ?>
                        Sélectionnez le <?= $schemaElement['params']['label']; ?>
                    <?php endif; ?>
                </option>
                <?php
                if(isset($schemaElement['params']['selectEnum'])):
                    static::renderSelectEnumOptions($schemaElement);
                elseif(isset($schemaElement['params']['Model']) && new $schemaElement['params']['Model']() instanceof AbstractDbClass):
                    static::renderSelectModelOptions($schemaElement);
                endif;
                ?>
            </select>
        </div>

        <?php
    }


    static public function renderElement($schemaElement, $params = [])
    {
        if($schemaElement['type'] == 'select') {
            static::renderSelect($schemaElement, $params);
            return;
        }

        static::renderInput($schemaElement, $params);
    }
}