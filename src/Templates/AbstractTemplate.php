<?php
namespace Project\Templates;

class AbstractTemplate {

    static function renderCondition($condition, $render = false, $renderFalse = false)
    {
        if($condition) {
            if(!$render) { return $condition; }
            return $render;
        }
        if($renderFalse !== false) { return $renderFalse; }
        return '';
    }
}