<?php
namespace Project\Models;

use Project\Core\BaseObject;
use Project\Dao\AbstractDao;
use Project\Dao\DynamicCache;
use Project\PageManager;
use Project\Utilities\SimpleDataPerfTest;

abstract class AbstractDbClass extends BaseObject {
    private $allowedProperties = [];

    public function save()
    {

        if(!$this->isComplete()) { return false; }
        if($this->getId()) {
            PageManager::getDao()->findDaoFromClass($this->getClassCompleteName())->{'update'.$this::getClassShortName()}($this);
            return true;
        }
        PageManager::getDao()->findDaoFromClass($this->getClassCompleteName())->{'create'.$this::getClassShortName()}($this);
        return true;
    }

    abstract public function isComplete(): bool;


    /**
     * @param $targetClass => target class to load
     * @param $sourceProperty => orignPropertyValue
     * @param null $forcedAutoload => should we autoload ?
     * @param null $sourcePropertyName => name of $sourceProperty, used to store the loaded result
     * @param string $distProperty => dist property for the condition, default is 'id'
     * @param string $oneTo => 'one' or 'many'
     * @param array $additional_conditions
     * @param array $additional_parameters
     */
    public function autoLoadObject($targetClass, $sourceProperty, $forcedAutoload = null, $sourcePropertyName = null, $distProperty = 'id', $oneTo = 'one', $additional_conditions = [], $additional_parameters = [] )
    {
        if(is_object($sourceProperty)) {
            if($forcedAutoload === false && method_exists($sourceProperty, 'getId')) {
                return $sourceProperty->getId();
            }
            SimpleDataPerfTest::incrementData('object_autoload_stored', 1);
            return $sourceProperty;
        }

        $oldAutoload = $this->isAutoloadObjects();

        if($forcedAutoload !== null) { $this->setAutoloadObjects($forcedAutoload); }

        if(!$this->isAutoloadObjects()) {
            $this->setAutoloadObjects($oldAutoload);
            return $sourceProperty;
        }

        $result = false;
        if($oneTo == 'many') { $result = []; }

        $className = (new \ReflectionClass($targetClass))->getShortName();
        $condition = [sprintf('%s = "%s"', $distProperty, $sourceProperty)];

        if($additional_conditions) {
            if(!is_array($additional_conditions)) { $additional_conditions = [$additional_conditions]; }
            $condition = array_merge($condition, $additional_conditions);
        }

        //We search inside the DynamicCache if the ID to autoload is already store !
        if($distProperty == 'id') {
            $result = DynamicCache::getModelFromCacheById($className, $sourceProperty);
        } else if($oneTo == 'many') {
            $result = DynamicCache::getResultsFromAdvancedCache($className, $condition);
        }

        /** @var AbstractDao $dao */
        $dao = PageManager::getDao()->findDaoFromClass($targetClass);

        if($oneTo == 'many') { $className = $dao->getMultipleName(); }
        if(!$result) {
            if(method_exists($dao, 'get'.$className.'ById')) {
                $result = $dao->{'get'.$className.'ById'}($sourceProperty);
            } else {
                $result = $dao->{'get'.$className}($condition, $additional_parameters);
                if($result === [] && $oneTo == 'one') {
                    $result = false;
                }
            }
        }

        if($sourcePropertyName && method_exists($this, 'set'.$sourcePropertyName)) {
            $this->{'set' . $sourcePropertyName}($result);
            SimpleDataPerfTest::incrementData('object_autoload_request', 1);
        } else if(method_exists($this, 'set'.$className)) {
            $this->{'set' . $className}($result);
            SimpleDataPerfTest::incrementData('object_autoload_request', 1);
        }

        $this->setAutoloadObjects($oldAutoload);
        return $result;
    }

    protected function addAllowedProperties($key, $props)
    {
        if(!is_array($props)) { $props = [$props]; }
        foreach($props as $prop) {
            if (!property_exists($this, $prop)) { continue; }
            if($this->isPropertyProtected($prop)) { continue; }

            $this->allowedProperties[$key][] = $prop;
        }
    }

    public function getAllowedProperties($key)
    {
        if(isset($this->allowedProperties[$key])) { return $this->allowedProperties[$key];}
        return [];
    }

    public function __toString()
    {
        if(method_exists($this, 'getId')) {
            return $this->getId();
        }
        return '';
    }
}